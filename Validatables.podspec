#
# Be sure to run `pod lib lint Validatables.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Validatables'
  s.version          = '0.1.4'
  s.summary          = 'A set of custom views and protocols for easy input validation.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: UITextField, UITextView and UIView children which have the ability to be validated and also change appearance/behavior on validation fail or success. ValidatorManager class that manages the validations and Validatable protocol that can be extended in case you want your custom view to be validatable. Includes ValidatableUISettings class to set global properties.
                       DESC

  s.homepage         = 'https://bitbucket.org/dyanko_yovchev/validatables.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'dyanko.yovchev' => 'dyanko.yovchev@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/dyanko_yovchev/validatables.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/Yovcheff'

  s.ios.deployment_target = '9.0'

  s.source_files = 'Validatables/Classes/**/*'
  
  # s.resource_bundles = {
  #   'Validatables' => ['Validatables/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
