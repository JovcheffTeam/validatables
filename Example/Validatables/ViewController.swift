//
//  ViewController.swift
//  Validatables
//
//  Created by dyanko.yovchev on 06/19/2017.
//  Copyright (c) 2017 dyanko.yovchev. All rights reserved.
//

import UIKit
import Validatables

class ViewController: UIViewController {
    @IBOutlet weak var validablePassword: ValidatableTextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        validablePassword.setValidationRule(message: "Password too short") { text in
            return text.characters.count > 6
        }
    }

    @IBAction func didTapValidate(_ sender: Any) {
        switch ValidatorManager().validate(forms: validablePassword) {
        case .ok():
            //...
            print("Field is valid")
        case .fail(let error):
            let alert = UIAlertController(title: "Fail", message: error, preferredStyle: .alert)
            present(alert, animated: true, completion: nil)
        }
    }
    
}

