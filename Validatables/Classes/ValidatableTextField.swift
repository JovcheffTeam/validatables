//
//  ValidatableTextField.swift
//  Skeleton
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

//    A UITextField child that implements the Validable protocol and can be
//    Passed to a ValidatorManager instance. Make sure to invoke
//    setValidationRule() to every instance of this class prior to validating it
public class ValidatableTextField: UITextField, Validatable, HighlightsOnFail {
    
// MARK: Validatable specific fields and methods
    internal (set) public var message: () -> String = {
        assertionFailure("Error in ValidableTextField: invoiking isValid() while validation rule not set!")
        return ""
    }
    
    internal (set) public var isValid: () -> Bool = { return false }
    
    public func setValidationRule(message: String, rule: @escaping (String) -> Bool) {
        self.isValid = {
            guard let text = self.text else { return false }
            return rule(text)
        }
        
        self.message = {
            return message
        }
        // Adding target for binding purposes
        self.addTarget(self, action: #selector(changed), for: .editingChanged)
    }
    
// MARK: Additional custom TextField fields and methods
    @IBInspectable var insetX: CGFloat = 5
    @IBInspectable var insetY: CGFloat = 0
    
    /// Customizable UI properties
    private (set) public var textClr = ValidatableUISettings.TextFieldProperties.textColor
    private (set) public var bgColor = ValidatableUISettings.TextFieldProperties.bgColor
    private (set) public var placeholderClr = ValidatableUISettings.TextFieldProperties.placeholderColor
    private (set) public var cornerRadius = ValidatableUISettings.TextFieldProperties.cornerRadius
    private (set) public var borderColor = ValidatableUISettings.TextFieldProperties.borderColor
    private (set) public var borderWidth = ValidatableUISettings.TextFieldProperties.borderWidth
    private (set) public var textFont = ValidatableUISettings.TextFieldProperties.font
    private (set) public var tintClr = ValidatableUISettings.TextFieldProperties.tintColor
    private (set) public var failedBackground: UIColor = ValidatableUISettings.TextFieldProperties.failedBackground
    private (set) public var failedBorderColor: CGColor = ValidatableUISettings.TextFieldProperties.failedBorderColor
    private (set) public var failedBorderWidth: CGFloat = ValidatableUISettings.TextFieldProperties.failedBorderWidth
    
    // If set to true the field validates againts its rule on editingChanged() and changes it's appearance accordingly
    private (set) public var autoValidate = ValidatableUISettings.TextFieldProperties.autoValidate
    
    private var onChange: ((String) -> Void)?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        applyStyles()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        applyStyles()
    }
    
    func applyStyles() {
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSForegroundColorAttributeName: placeholderClr])
        self.textColor = textClr
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        layer.borderColor = borderColor
        layer.borderWidth = borderWidth
        self.font = textFont
        self.tintColor = tintClr
    }
    
    @discardableResult
    public func customize(textClr: UIColor = ValidatableUISettings.TextFieldProperties.textColor,
                   bgColor: UIColor = ValidatableUISettings.TextFieldProperties.bgColor,
                   placeholderClr: UIColor = ValidatableUISettings.TextFieldProperties.placeholderColor,
                   cornerRadius: CGFloat = ValidatableUISettings.TextFieldProperties.cornerRadius,
                   borderColor: CGColor = ValidatableUISettings.TextFieldProperties.borderColor,
                   borderWidth: CGFloat = ValidatableUISettings.TextFieldProperties.borderWidth,
                   textFont: UIFont = ValidatableUISettings.TextFieldProperties.font,
                   tintClr: UIColor = ValidatableUISettings.TextFieldProperties.tintColor,
                   failedBackground: UIColor = ValidatableUISettings.TextFieldProperties.failedBackground,
                   failedBorderColor: CGColor = ValidatableUISettings.TextFieldProperties.failedBorderColor,
                   failedBorderWidth: CGFloat = ValidatableUISettings.TextFieldProperties.failedBorderWidth,
                   autoValidate: Bool = ValidatableUISettings.TextFieldProperties.autoValidate) -> Self {
        
        self.textClr = textClr
        self.bgColor = bgColor
        self.placeholderClr = placeholderClr
        self.cornerRadius = cornerRadius
        self.borderColor = borderColor
        self.borderWidth = borderWidth
        self.textFont = textFont
        self.tintClr = tintClr
        self.failedBackground = failedBackground
        self.failedBorderColor = failedBorderColor
        self.failedBorderWidth = failedBorderWidth
        self.autoValidate = autoValidate
        
        applyStyles()
        return self
    }
    
    @discardableResult
    func bind(closure: @escaping (String) -> Void) -> Self {
        self.onChange = closure
        return self
    }
    
    public func getBorderColor(for state: Bool) -> CGColor {
        return state ? borderColor : failedBorderColor
    }
    
    public func getBackgroundColor(for state: Bool) -> UIColor {
        return state ? bgColor : failedBackground
    }
    
    public func getBorderWidth(for state: Bool) -> CGFloat {
        return state ? borderWidth : failedBorderWidth
    }
    
    internal func changed() {
        if let text = self.text {
            if autoValidate {
                setValidationState(success: isValid())
            }
            onChange?(text)
        }
    }
    
    override public func didChange(_ changeKind: NSKeyValueChange, valuesAt indexes: IndexSet, forKey key: String) {
        // TODO: Capture change and trigger event
        super.didChange(changeKind, valuesAt: indexes, forKey: key)
    }
    
    // placeholder position
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    // text position
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
}
