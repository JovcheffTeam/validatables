//
//  ValidatableUISettings.swift
//  Skeleton
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

public class ValidatableUISettings {
    //// Default values, can be overriden for global control. Every Validatable component's UI is defaulted to these fields.
    
    public struct TextFieldProperties {
        public static var textColor = UIColor.black
        public static var bgColor = UIColor.white
        public static var placeholderColor = UIColor.gray
        public static var cornerRadius: CGFloat = 3
        public static var borderColor = UIColor.clear.cgColor
        public static var borderWidth: CGFloat = 0
        public static var font = UIFont.systemFont(ofSize: 16, weight: 0.25)
        public static var tintColor = UIColor.blue
        public static var failedBackground = UIColor.white
        public static var failedBorderColor = UIColor.red.cgColor
        public static var failedBorderWidth: CGFloat = 1
        public static var autoValidate = false
    }
    
    public struct TextViewProperties {
        public static var bgColor = UIColor.white
        public static var borderColor = UIColor.clear.cgColor
        public static var borderWidth: CGFloat = 0
        public static var failedBackground = UIColor.white
        public static var failedBorderColor = UIColor.red.cgColor
        public static var failedBorderWidth: CGFloat = 1
        public static var autoValidate = false
    }
    
    public struct UIViewProperties {
        public static var bgColor = UIColor.white
        public static var borderColor = UIColor.clear.cgColor
        public static var borderWidth: CGFloat = 0
        public static var failedBackground = UIColor.white
        public static var failedBorderColor = UIColor.red.cgColor
        public static var failedBorderWidth: CGFloat = 1
        public static var autoValidate = false
    }
    
}
