//
//  FormValidator.swift
//  Skeleton
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

typealias DataTuple = (name: String, age: Int, favorites: [String: String])

// A protocol ensuring the object can be passed to the ValidatorManager instance and has the isValid and message property
public protocol Validatable {
    var message: () -> String { get }
    var isValid: () -> Bool { get }
    func getBorderColor(for state: Bool) -> CGColor
    func getBackgroundColor(for state: Bool) -> UIColor
    func getBorderWidth(for state: Bool) -> CGFloat
    func setValidationRule(message: String, rule: @escaping (String) -> Bool)
}

public class ValidatorManager {
    private (set) var failedValidatables: [Validatable] = []
    let shouldHighlightFailed: Bool
    
    public init(highlighting: Bool = true) {
        self.shouldHighlightFailed = highlighting
    }
    
    @discardableResult
    public func validate(forms: Validatable...) -> ValidationResult {
        var failureMessage = ""
        var result = true
        
        failedValidatables.removeAll()

        for form in forms {
            // Coloring the form to be validated.
            (form as? HighlightsOnFail)?.setValidationState(success: form.isValid())
            if !form.isValid() {
                failureMessage += form.message() + "\n"
                result = false
                failedValidatables.append(form)
            }
        }
        
        let failureMessageTrimmed = failureMessage.trimmingCharacters(
            in: NSCharacterSet(charactersIn: "\n") as CharacterSet)
        
        return result ? .ok() : .fail(failureMessageTrimmed)
    }
}

public enum ValidationResult {
    case ok()
    case fail(String)
}

public protocol HighlightsOnFail {
    func setValidationState(success: Bool)
}

extension HighlightsOnFail where Self: Validatable {
    public func setValidationState(success: Bool) {
        guard let sView = self as? UIView else { return }
        
        sView.layer.borderColor = self.getBorderColor(for: success)
        sView.backgroundColor = self.getBackgroundColor(for: success)
        sView.layer.borderWidth = self.getBorderWidth(for: success)
    }
}
