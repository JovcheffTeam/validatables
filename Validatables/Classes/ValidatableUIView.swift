//
//  ValidatableUIView.swift
//  Skeleton
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

public class ValidatableUIView: UIView, Validatable, HighlightsOnFail {
    internal (set) public var message: () -> String = {
        assertionFailure("Error in ValidableTextField: invoiking isValid() while validation rule not set!")
        return ""
    }
    
    internal (set) public var isValid: () -> Bool = { return false }
    
    public func setValidationRule(message: String, rule: @escaping (String) -> Bool) {
        self.isValid = {
            return rule("")
        }
        
        self.message = {
            return message
        }
    }
    
    public func getBorderColor(for state: Bool) -> CGColor {
        return state ? ValidatableUISettings.TextViewProperties.borderColor : ValidatableUISettings.TextViewProperties.failedBorderColor
    }
    
    public func getBackgroundColor(for state: Bool) -> UIColor {
        return state ? ValidatableUISettings.TextViewProperties.bgColor : ValidatableUISettings.TextViewProperties.failedBackground
    }
    
    public func getBorderWidth(for state: Bool) -> CGFloat {
        return state ? ValidatableUISettings.TextViewProperties.borderWidth : ValidatableUISettings.TextViewProperties.failedBorderWidth
    }
}
