# Validatables

[![CI Status](http://img.shields.io/travis/dyanko.yovchev/Validatables.svg?style=flat)](https://travis-ci.org/dyanko.yovchev/Validatables)
[![Version](https://img.shields.io/cocoapods/v/Validatables.svg?style=flat)](http://cocoapods.org/pods/Validatables)
[![License](https://img.shields.io/cocoapods/l/Validatables.svg?style=flat)](http://cocoapods.org/pods/Validatables)
[![Platform](https://img.shields.io/cocoapods/p/Validatables.svg?style=flat)](http://cocoapods.org/pods/Validatables)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Validatables is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Validatables"
```

## Author

dyanko.yovchev, dyanko.yovchev@imperiaonline.org

## License

Validatables is available under the MIT license. See the LICENSE file for more info.
